# Weather Forecast

Prvi premteni projekat iz predmeta 'Interakcija čovek računar', aplikacija koja koristi tabele i grafikone radi prikaza vremenske prognoze

Tehnologija:
 Spring-Boot Maven projekat + jQuery

Pokretanje projekta:
 1) Pokrenuti Eclipse EE i u novi/postojeći workspace importovati projekat: 
		File -> Import -> Maven -> Existing Maven Project i odabrati ovaj projekat
 2) Instaliranje svih zavisnosti iz pom.xml fajla:
		Desni klik na projekat -> Run as -> Maven Build -> nakon otvaranja prozora u Goals navesti install -> Run
 3) Pokretanje programa:
		desni klik na "weather-forecast/src/main/java/com.hci.project.weatherforecast/WeatherForecastApplication.java" -> Run As -> Java Application
 4) Klijentska strana je browser, adresa je:
		localhost:8080

Zavisnosti (pom.xml):
 1) org.springframework.boot spring-boot-starter-web
 2) org.springframework.boot spring-boot-starter-test
 2) org.json

Biblioteke (front):
 1) jQuery
 2) Select2
 3) Chart.js