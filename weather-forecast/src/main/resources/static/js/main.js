// global weather chart
var weatherChart;
var chartData;
var chartLabel;

// color palette
var colorPalette = ["#4d9cb3", "#fa4251", "#00b8de", "#98909f", "#cbb894", "#003579", "#017e5c", "#71c100", "#f3d100", "#bf5b48"]

// color coounter
var colorCount = 0;

$(document).ready(function() {

  // city select initialisation
  $(".js-example-placeholder-multiple").select2({
    placeholder: "Search and select a city, or a few...",
    width: "100%",
    dropdownAutoWidth : true,
  });
  // forecast type select initialisation
  $('.js-example-basic-single').select2({
    minimumResultsForSearch: -1,
    width: "100%",
    dropdownAutoWidth : true,
  });

  // start button
  $('#start').click(function(){
    var cities = $('#citySelect').select2('data')
    if (cities) {
      $('#mainBody').html('')
      $('#chartCanvas').remove()
      $('#weatherChart').append('<canvas id="chartCanvas" class="chart-canvas"></canvas>')
    }
    if (cities.length != 0) {
        initialiseCitiesData(cities)
    }
    });

    // radio-button change
    $('input:radio[name=unitType]').change(function() {
      var cities = $('#citySelect').select2('data')
      $('#mainBody').html('')
      $('#chartCanvas').remove()
      $('#weatherChart').append('<canvas id="chartCanvas" class="chart-canvas"></canvas>')
      if (cities.length != 0) {
        initialiseCitiesData(cities)
      }
    })

  })

function initialiseCitiesData(cities) {
  // wait for http request and response
  $("body").css("cursor", "wait");
  // reset color counter
  colorCount = 0
  makeChartWeather()
  $.each(cities, function( index, city ) {
    $.ajax({
      type: "POST",
      async: false,
      url: "/api/cityInfo?cityName=" + city.id,
      contentType : "application/json",
      dataType : "json",
      data : city.id,
      success: function(data){
        fillCityWeather(city.id, data);
        fillChartWeather(city.id);
      },
    error : function(response) {
      alert(response.responseText);
      }
    })
  })
  // waiting ended
  $("body").css("cursor", "default");
}

function fillCityWeather(cityName, data) {
  var forecastType = $('#forecastType').select2('data')[0].id

  // add city to table
  var tableBody = $(document).find('#mainBody')
  var cityRow = $('<tr id='+cityName+' class="detailed-view visible'+cityName.replace(/\s/g, '')+'">'+
                    '<td>'+cityName+'</td>'+
                    '<td>'+Math.round(data.current.temp)+' °C</td>'+
                  '</tr>')
  tableBody.append(cityRow)

  if (forecastType == "Daily") {
    // hourly forecast for 24 hours
    fillHourly(cityName, data)
  } else if (forecastType == "Two Day") {
    // two day forecast
    fillTwoDay(cityName, data)
  } else {
    // weekly forecast
    fillWeekly(cityName, data)
  }

  // hiding detail rows
  $('.hidden-row').hide()

  // show hidden detail rows
  $('tr.detailed-view.visible'+cityName.replace(/\s/g, '')).click(function(e){
    e.preventDefault();
    var cityRow = $(e.target).parent()
    var city = $(e.target).parent()["0"].id
    if (cityRow.hasClass('active')) {
      // vec bio kliknut
      // treba da se sklone redovi
      $('.'+city).hide()
      // uklanjamo klasu
      cityRow.removeClass('active')
    } else {
      // nije bio kliknut
      // treba da se pojave redovi
      $('.'+city).show()
      // dodajemo klasu
      cityRow.addClass('active')
    }
  })
}

function fillHourly(cityName, data) {
  var tableBody = $(document).find('#mainBody')
  var hiddenRow = $('<tr class="'+cityName+' hidden-row"></tr>')
  var hiddenCell = $('<td colspan="2"></td>')

  var table = $('<table class="table table-hover table-striped table-valign-middle">'+
                  '<thead class="hidden">' +
                    '<th>Time</th><th>Temp (°C)</th><th>Air pressure (hPa)</th><th>Humidity (%)</th><th>Wind speed (m/s)</th>' +
                  '</thead>' +
                  '<tbody>' +
                  '</tbody>' +
                '</table>')

  var hourly = data.hourly

  var currentHour = new Date().getHours()
  
  // prepare data and label for chart
  chartLabel = []
  chartData = {
    temp: [],
    pressure: [],
    humidity: [],
    windSpeed: []
  };

  $.each(hourly, function(index, hourData) {
      if (index > 23) {
        return false;
      }
      var row = $('<tr></tr>')
      var time = currentHour + index
      // 24 hour
      if (time >= 24) {
        time = time - 24
      }
      if (index == 0) {
        time = "Now"
      } else {
        time = time + ":00"
      }
      row.append('<td>'+time+'</td>' +
                 '<td>'+Math.round(hourData.temp)+' °C</td>' +
                 '<td>'+hourData.pressure+' hPa</td>' +
                 '<td>'+hourData.humidity+' %</td>' +
                 '<td>'+hourData.wind_speed+' m/s</td>')
      table.append(row)
      // add time to chart label
      chartLabel.push(time)
      // add data to chart data
      chartData.temp.push(Math.round(hourData.temp))
      chartData.pressure.push(hourData.pressure)
      chartData.humidity.push(hourData.humidity)
      chartData.windSpeed.push(hourData.wind_speed)
  })

  hiddenCell.append(table)
  hiddenRow.append(hiddenCell)
  tableBody.append(hiddenRow)
}

function fillTwoDay(cityName, data) {
  var tableBody = $(document).find('#mainBody')
  var hiddenRow = $('<tr class="'+cityName+' hidden-row"></tr>')
  var hiddenCell = $('<td colspan="2"></td>')

  var table = $('<table class="table table-hover table-striped table-valign-middle">'+
                  '<thead class="hidden">' +
                    '<th>Day</th><th>Time</th><th>Temp (°C)</th><th>Air pressure (hPa)</th><th>Humidity (%)</th><th>Wind speed (m/s)</th>' +
                  '</thead>' +
                  '<tbody>' +
                  '</tbody>' +
                '</table>')

  var hourly = data.hourly

  var today = new Date()
  var tommorow = new Date()
  tommorow.setDate(today.getDate() + 1)
  var date = new Date()
  var currentHour = date.getHours()
  var day = date.toLocaleDateString('en-US', { weekday: 'short' })

  // prepare data and label for chart
  chartLabel = []
  chartData = {
    temp: [],
    pressure: [],
    humidity: [],
    windSpeed: []
  };

  $.each(hourly, function(index, hourData) {
      var row = $('<tr></tr>')
      var time = currentHour + index
      // 24 hour
      if (time >= 24) {
        time = time - 24
        if (today.getTime() == date.getTime()) {
          date.setDate(date.getDate() + 1)
          day = date.toLocaleDateString('en-US', { weekday: 'short' })
        }
      }
      // 48 hour
      if (time >= 24) {
        time = time - 24
        if (tommorow.getTime() == date.getTime()) {
          date.setDate(date.getDate() + 1)
          day = date.toLocaleDateString('en-US', { weekday: 'short' })
        }
      }
      row.append('<td>'+day+'</td>' +
                 '<td>'+time+':00</td>' +
                 '<td>'+Math.round(hourData.temp)+' °C</td>' +
                 '<td>'+hourData.pressure+' hPa</td>' +
                 '<td>'+hourData.humidity+' %</td>' +
                 '<td>'+hourData.wind_speed+' m/s</td>')
      table.append(row)

      // add time to chart label
      chartLabel.push((day+" "+time+":00"))
      // add data to chart data
      chartData.temp.push(Math.round(hourData.temp))
      chartData.pressure.push(hourData.pressure)
      chartData.humidity.push(hourData.humidity)
      chartData.windSpeed.push(hourData.wind_speed)
  })

  hiddenCell.append(table)
  hiddenRow.append(hiddenCell)
  tableBody.append(hiddenRow)
}

function fillWeekly(cityName, data) {
  var date = new Date()

  var tableBody = $(document).find('#mainBody')
  var hiddenRow = $('<tr class="'+cityName+' hidden-row"></tr>')
  var hiddenCell = $('<td colspan="2"></td>')

  var table = $('<table class="table table-hover table-striped table-valign-middle">'+
                  '<thead class="hidden">' +
                    '<th>Day</th><th>Min Temp (°C)</th><th>Max Temp (°C)</th><th>Air pressure (hPa)</th><th>Humidity (%)</th><th>Wind speed (m/s)</th>' +
                  '</thead>' +
                  '<tbody>' +
                  '</tbody>' +
                '</table>')

  var daily = data.daily

  // prepare data and label for chart
  chartLabel = []
  chartData = {
    temp: [],
    pressure: [],
    humidity: [],
    windSpeed: []
  };

  $.each(daily, function(index, dayData) {
      day = date.toLocaleDateString('en-US', { weekday: 'short' })
      if (index == 0) {
        day = "Today"
      }
      var row = $('<tr></tr>')
      row.append('<td>'+day+'</td>' +
                 '<td>'+Math.round(dayData.temp.min)+' °C</td>' +
                 '<td>'+Math.round(dayData.temp.max)+' °C</td>' +
                 '<td>'+dayData.pressure+' hPa</td>' +
                 '<td>'+dayData.humidity+' %</td>' +
                 '<td>'+dayData.wind_speed+' m/s</td>')
      table.append(row)
      date.setDate(date.getDate() + 1)
      
      // add day to chart label
      chartLabel.push(day)
      // add data to chart data
      chartData.temp.push(Math.round(dayData.temp.day))
      chartData.pressure.push(dayData.pressure)
      chartData.humidity.push(dayData.humidity)
      chartData.windSpeed.push(dayData.wind_speed)
  })

  hiddenCell.append(table)
  hiddenRow.append(hiddenCell)
  tableBody.append(hiddenRow)
}

function rowClick() {
  // hiding detail rows
  $('.hidden-row').hide()

  // show hidden detail rows
  $('tr.detailed-view').click(function(e){
    e.preventDefault();
    var cityRow = $(e.target).parent()
    var city = $(e.target).parent()["0"].id
    if (cityRow.hasClass('active')) {
      // vec bio kliknut
      // treba da se sklone redovi
      $('.'+city).hide()
      // uklanjamo klasu
      cityRow.removeClass('active')
    } else {
      // nije bio kliknut
      // treba da se pojave redovi
      $('.'+city).show()
      // dodajemo klasu
      cityRow.addClass('active')
    }
  })
}

function makeChartWeather() {
  if (weatherChart) {
    weatherChart.destroy()
  }
  var context = $('#chartCanvas')
  var forecastType = $('#forecastType').select2('data')[0].id
  var xLabel;
  if (forecastType == "Weekly") {
    xLabel = "Time (days)"
  } else {
    xLabel = "Time (h)"
  }
  var unit = $('input[name="unitType"]:checked').val()
  weatherChart = new Chart(context, {
    type: 'line',
    options: {
      scales: {
        yAxes: [{
          scaleLabel: {
            display: true,
            labelString: unit
          }
        }],
        xAxes: [{
          scaleLabel: {
            display: true,
            labelString: xLabel
          }
        }]
      }
    }
  })
}

function fillChartWeather(cityName) {
  // get color
  var color = colorPalette[colorCount]
  colorCount++;
  // add labels
  weatherChart.data.labels = chartLabel

  var currentData;
  var unit = $('input[name="unitType"]:checked').val()
  if (unit.includes("Temperature")) {
    currentData = chartData.temp
  } else if (unit.includes("Humidity")) {
    currentData = chartData.humidity
  } else if (unit.includes("Air pressure")) {
    currentData = chartData.pressure
  } else {
    currentData = chartData.windSpeed
  }

  weatherChart.data.datasets.push({
    label: cityName,
     // line and label color
		 backgroundColor: color,
     borderColor: color,
		 pointHoverRadius: 5,
     // points color
		 pointBackgroundColor: color,
     pointBorderColor: color,
		 // just line or filled
		 fill: false,
		 data: currentData
  })

  weatherChart.update()
}