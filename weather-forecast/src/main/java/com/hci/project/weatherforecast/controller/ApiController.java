package com.hci.project.weatherforecast.controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import org.json.JSONObject;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class ApiController {
	
	private static final String apiBaseURL = "http://api.openweathermap.org/data/2.5/";
	private static final String apiLocation = "weather?q=";
	private static final String apiWeatherLat = "onecall?lat=";
	private static final String apiWeatherLon= "&lon=";
	private static final String apiUnit = "&units=metric";
	private static final String apiID = "&appid=cea0ba70c4226a945f6045bfc44a19e2";
	
	@PostMapping(value = "/cityInfo", produces = MediaType.APPLICATION_JSON_VALUE)
	public String getCityInfo(@RequestParam(name = "cityName") String cityName) throws IOException {
		// dobavljanje koordinata potrebnih za vremenski prognozu
		try {
			URL url = new URL(apiBaseURL + apiLocation + cityName + apiID);
			HttpURLConnection connection = (HttpURLConnection) url.openConnection();
			// slanje zahteva
			connection.setRequestMethod("GET");
			connection.setDoInput(true);
			connection.setDoOutput(true);
			connection.setRequestProperty("Accept", "application/json");
			connection.connect();
			// primanje odgovora
			StringBuffer buffer = new StringBuffer();
			BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream()));
			String line;
			while ((line = br.readLine()) != null) {
				buffer.append(line);
			}
			// raskidanje konekcije
			connection.disconnect();
			
			// dobavljanje lokacije
			JSONObject apiResponse = new JSONObject(buffer.toString());
			String lon = apiResponse.getJSONObject("coord").get("lon").toString();
			String lat = apiResponse.getJSONObject("coord").get("lat").toString();
			
			// dobavljanje vremenske prognoze
			url = new URL(apiBaseURL + apiWeatherLat + lat + apiWeatherLon + lon + apiUnit + apiID);
			connection = (HttpURLConnection) url.openConnection();
			// slanje zahteva
			connection.setRequestMethod("GET");
			connection.setDoInput(true);
			connection.setDoOutput(true);
			connection.setRequestProperty("Accept", "application/json");
			connection.connect();
			// primanje odgovora
			buffer = new StringBuffer();
			br = new BufferedReader(new InputStreamReader(connection.getInputStream()));
			while ((line = br.readLine()) != null) {
				buffer.append(line);
			}
			// raskidanje konekcije
			connection.disconnect();
			
			// kreces kod ajaxa od api
			return buffer.toString();
		} catch (MalformedURLException e) {
			e.printStackTrace();
			return "There has been an ";
		}
	}
}
